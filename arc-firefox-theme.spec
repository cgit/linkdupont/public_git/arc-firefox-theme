%global		debug_package	%{nil}
%global		gittag		50.20161114

Name:		arc-firefox-theme
Version:	50.20161114
Release:	1%{?dist}
Summary:	Arc theme for Firefox

License:	MPLv2.0
URL:		https://github.com/horst3180/arc-firefox-theme
Source0:	https://github.com/horst3180/arc-firefox-theme/archive/%{gittag}.tar.gz#/%{name}-%{version}.tar.gz
Source1:	firefox-fedora-budgie-spin-default-prefs.js

BuildArch:	noarch

BuildRequires:	autoconf
BuildRequires:	automake
Requires:	firefox

%description
%{summary}.


%prep
%autosetup


%build
NOCONFIGURE=1 ./autogen.sh
%configure
make mkxpi


%install
%make_install
find %{buildroot} -type f -iname *.svg -exec chmod a-x '{}' \;
install -Dm00644 %SOURCE1 %{buildroot}%{_libdir}/firefox/browser/defaults/preferences

%files
%license LICENSE
%doc README.md
%{_libdir}/firefox

%changelog
* Sun Nov 27 2016 Link Dupont <linkdupont@fedoraproject.org> - 50.20161114-1
- Initial package
